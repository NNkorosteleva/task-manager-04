# TASK-MANAGER-04

## DEVELOPER INFO

* **NAME**: Anastasia Korosteleva

* **E-MAIL**: nnkorosteleva@gmail.com

* **E-MAIL**: akorosteleva@t1-consulting.ru

## SOFTWARE

* OpenJDK 8

* Intellij Idea

* Windows 10

## HARDWARE

* **RAM**: 16GB

* **CPU**: i5

* **HDD**: 476GB

## RUN PROGRAM

```bush
java -jar ./task-manager.jar
```
